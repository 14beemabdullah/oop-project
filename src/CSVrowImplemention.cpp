#include <iostream>

#include "CSVrow.h"

using namespace std;



void  Cordinates::setx1(float x)
{
	x1 = x;
}

void  Cordinates::setx2(float x)
{
	x2 = x;
}

void  Cordinates::sety1(float x)
{
	y1 = x;
}

void  Cordinates::sety2(float x)
{
	y2 = x;
}

float  Cordinates::getx1()
{
	return x1;
}

float  Cordinates::getx2()
{
	return x2;
}

float  Cordinates::gety1()
{
	return y1;
}

float  Cordinates::gety2()
{
	return y2;
}



std::size_t CSVRow::size() const
{
			return m_data.size();
}



void CSVRow::readNextRow(std::istream& str)

{
	std::string line; 
	std::getline(str, line);
	std::stringstream lineStream(line);
	std::string cell; 
	m_data.clear();
	while (std::getline(lineStream, cell, ','))
	{
		m_data.push_back(cell);
	}

}


std::string const& CSVRow::operator[](std::size_t index) const
{
			return m_data[index];
}



std::istream& operator>>(std::istream& str, CSVRow& data)
{
	data.readNextRow(str);
	return str;
}


