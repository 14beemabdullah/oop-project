#include "VideoCap.h"

using namespace std;
using namespace cv;

void rotateImage(cv::Mat& src, double angle, cv::Mat& dst)
{
    int len = std::max(src.cols, src.rows);
    cv::Point2f pt(len/2., len/2.);
    cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);

    cv::warpAffine(src, dst, r, cv::Size(len, len));
}

int FaceDetector(string &classifier){

	
	CascadeClassifier face_cascade;
	string window = "Capture - face detection";

	if (!face_cascade.load(classifier))
	{
		cout << " Error loading cascade file." << endl;
		return -1;
	}

	VideoCapture cap(0);
	

	if (!cap.isOpened())
	{
		cout << "Error: Camera failed to open." << endl;
		cin.get();
		return -1;
	}


	namedWindow(window, 1);
	long counter = 0;

	
	while (true)
	{
		vector<Rect> faces;
		Mat frame;
		Mat greyScaleFrame;
		Mat cropImg;
		Mat equalizedFrame;

		cap >> frame;
		counter++;         

		if (!frame.empty()){


		   cvtColor(frame, greyScaleFrame, CV_BGR2GRAY);
		   equalizeHist(greyScaleFrame, greyScaleFrame);

			face_cascade.detectMultiScale(greyScaleFrame, faces, 1.1, 3, 0, cv::Size(190, 190), cv::Size(200, 200));

			cout << faces.size() << " faces detected" << endl;
			std::string frameset = std::to_string(counter);
			std::string faceset = std::to_string(faces.size());

			int width = 0, height = 0;

			cv::Rect region;

			for (int i = 0; i < faces.size(); i++)
			{
				rectangle(greyScaleFrame, Point(faces[i].x, faces[i].y), Point(faces[i].x + faces[i].width, faces[i].y + faces[i].height), Scalar(255, 0, 255), 1, 1, 0);
				cout << "Width: " << faces[i].width << "      Height: " << faces[i].height << endl;
				width = faces[i].width; height = faces[i].height;

			
				region.x = faces[i].x; region.width = faces[i].width;
				region.y = faces[i].y; region.height = faces[i].height;

	
             	cropImg = greyScaleFrame(region);
				cv::imshow("region", cropImg);
                

			}

			std::string wi = std::to_string(width);
			std::string he = std::to_string(height);

			cv::putText(greyScaleFrame, "Frames: " + frameset, cvPoint(30, 30), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0, 255, 0), 1, CV_AA);
			cv::putText(greyScaleFrame, "Faces Detected: " + faceset, cvPoint(30, 60), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0, 255, 0), 1, CV_AA);
			cv::putText(greyScaleFrame, "Resolution " + wi + " x " + he, cvPoint(30, 90), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0, 255, 0), 1, CV_AA);
			

			cv::imshow(window, greyScaleFrame);
		}
		if (waitKey(30) >= 0) break;
	}
}

int videoCapturing()
{
	VideoCapture cap(0);

	if (!cap.isOpened())
	{
		cout << "camera is not opened" << endl;
		return -1;
	}

	Mat edges;
	namedWindow("edges", 1);
	for (;;)
	{
		Mat frame;
		cap >> frame; 
		if (!frame.empty()) {
			cvtColor(frame, edges, CV_HLS2BGR);
			GaussianBlur(edges, edges, Size(7, 7), 1.5, 1.5);
			Canny(edges, edges, 0, 30, 3);
			imshow("edges", edges);
		}
		if (waitKey(30) == 10) break;
	}

	return 0;
}

int videoCapOriginal()
{
	
	VideoCapture cap(0);
	if (!cap.isOpened())
	{
		cout << "exit" << endl;
		return -1;
	}

	int counter = 0;
	string name;
	while (true)
	{
		name = counter + " window ";
		namedWindow(name, 1);

		Mat frame;
		cap >> frame;
		
		if (!frame.empty()){
			
			imshow(name, frame);
		}
		if (waitKey(30) >= 0) break;
	}
	return 0;
}




int photo_face_detector(string &classifier)
{
	
	CascadeClassifier face_cascade;
	
	if (!face_cascade.load(classifier))
	{ 
		printf("--(!)Error loading\n"); 
		return -1; 
	};


	Cordinates C;
	ifstream inFile("C:/database/train data/training1/Data.csv");

	if ( inFile.is_open() )
	{
		cout << "File is now open!\n\n\n" << endl;
	}

	else
	{
		cout << "Hahah! File Couldn't be opened!";
		cin.get();
		exit(1);
	}




	CSVRow row;
	Eye objEye;

	while (inFile >> row)
	{
		//SETTING VALUES USING SETTERS
		// x1,y1,x2,y2

		C.setx1 ( stof ( row[1] ) );
		C.sety1 ( stof (row[2] ) );
		C.setx2 ( stof ( row[3] ) );
		C.sety2 ( stof ( row[4] ) );
			
		objEye.vectorCordinates.push_back(C);

		string path1 = "C:/database/train data/training1/";
		string path2 = row[0];

		cout << "\n\nrow[0] = " << row[0];
		string image_completePath = path1 + path2;
		cout << "\n" << image_completePath;
		Mat  frame, rotated;
		frame = imread(image_completePath);
	
		if (!frame.empty())
		{
		double num =  C.gety1() - C.gety2();
		double denom = C.getx1() - C.getx2();
		double ratio = num / denom;
		double angle_rad = atan(ratio);
		double angle = angle_rad*57.13;
		
		cout << "Initial: " << angle;

		
		rotateImage(frame, angle , rotated);
		
		

		cv::imwrite(image_completePath, rotated);

		Mat greyScaleFrame;
		Mat cropImg;
		vector<Rect> faces;

		face_cascade.detectMultiScale(frame, faces, 1.1, 3, 0, cv::Size(190, 190), cv::Size(200, 200));


		int width = 0, height = 0;

		cv::Rect region;
		cout << "\nNumber of faces: " << faces.size();
		for (int i = 0; i < faces.size(); i++)
	{
				rectangle(frame, Point(faces[i].x, faces[i].y), Point(faces[i].x + faces[i].width, faces[i].y + faces[i].height), Scalar(255, 0, 255), 1, 1, 0);
				cout << "Width: " << faces[i].width << "      Height: " << faces[i].height << endl;
				width = faces[i].width; height = faces[i].height;


				region.x = faces[i].x; region.width = faces[i].width;
				region.y = faces[i].y; region.height = faces[i].height;
				cout << "\t\tCropping image....";
				cropImg = frame(region);
				
				cv::imwrite(image_completePath, cropImg);

	}


		}

		else
		{
			cout << "The frame is empty. Image not found!";
			cin.get();
			
		}

	}

	
		
	
		
	return 0;

//
//CascadeClassifier face_cascade;
//	
//	if (!face_cascade.load(classifier))
//	{ 
//		printf("--(!)Error loading\n"); 
//		return -1; 
//	};
//
//
//	Cordinates C;
//	ifstream inFile("C:\\database\\train data\\training8\\Data.csv");
//	if ( inFile.is_open() )
//	{
//		cout << "File is now open!\n\n\n" << endl;
//	}
//
//	else
//	{
//		cout << "File Couldn't be opened!";
//		cin.get();		exit(1);
//	}
//
//
//
//	CSVRow row;
//	Eye objEye;
//	while (inFile >> row)
//
//	{
//		SETTING VALUES USING SETTERS
//		
//		C.setx1 ( stof ( row[1] ) );
//		C.setx2 ( stof (row[2] ) );
//		C.sety1 ( stof ( row[3] ) );
//		C.sety2 ( stof ( row[4] ) );
//			
//
//		objEye.vectorCordinates.push_back(C);
//	}
//
//	
//	Mat  frame, rotated;
//	frame = imread("C:/database/train data/training8/01.png");
//
//
//	if (!frame.empty())
//	{	
//		
//		/*Mat greyScaleFrame;
//		Mat cropImg;
//		vector<Rect> faces;*/
//		rotateImage(frame, 90, rotated);
//		cv::imwrite("C:/database/train data/training8/01.png", rotated);
//		
//
//	/*	face_cascade.detectMultiScale(frame, faces, 1.1, 3, 0, cv::Size(190, 190), cv::Size(200, 200));
//
//
//		int width = 0, height = 0;
//
//			cv::Rect region;
//			cout << "Number of faces: " << faces.size();*/
//			for (int i = 0; i < faces.size(); i++)
//			{
//				rectangle(frame, Point(faces[i].x, faces[i].y), Point(faces[i].x + faces[i].width, faces[i].y + faces[i].height), Scalar(255, 0, 255), 1, 1, 0);
//				cout << "Width: " << faces[i].width << "      Height: " << faces[i].height << endl;
//				width = faces[i].width; height = faces[i].height;
//
//
//				region.x = faces[i].x; region.width = faces[i].width;
//				region.y = faces[i].y; region.height = faces[i].height;
//
//				cropImg = frame(region);
//				cv::imshow("Cropped and rotated", cropImg);			
//                cv::imwrite("image.png", cropImg);
//
//			}
//
//				
//			imshow("original", frame);
//
//		
//	}
//
//
//	else
//	{	
//	cout << "Error";
//	cin.get();
//	}
//
//	return 0;

}
