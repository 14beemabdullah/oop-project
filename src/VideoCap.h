#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include "opencv2\objdetect\objdetect.hpp"
#include <opencv2\opencv.hpp>
#include <opencv2\core\core.hpp>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <cmath>
#include "CSVrow.h"
#include <sstream>
#include <fstream>


int videoCapturing();
int videoCapOriginal();

int FaceDetector(std::string&);
int photo_face_detector(std::string &classifier);
void rotateImage(cv::Mat& src, double angle, cv::Mat& dst);
