//this one

#include <iostream>
#include <string>
#include "opencv2\core\core.hpp"
#include "opencv2\contrib\contrib.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\objdetect\objdetect.hpp"
#include "opencv2\opencv.hpp"
#include <fstream>
#include <sstream>

using namespace std;
using namespace cv;

static Mat MatNorm(InputArray _src) {
	Mat src = _src.getMat();
	// Create and return normalized image:
	Mat dst;
	switch (src.channels()) {
	case 1:
		cv::normalize(_src, dst, 0, 255, NORM_MINMAX, CV_8UC1);
		break;
	case 3:
		cv::normalize(_src, dst, 0, 255, NORM_MINMAX, CV_8UC3);
		break;
	default:
		src.copyTo(dst);
		break;
	}
	return dst;
}



Mat unsharpMask(cv::Mat& im)
{
	cv::Mat tmp;
	cv::GaussianBlur(im, tmp, cv::Size(5, 5), 5);
	cv::addWeighted(im, 1.5, tmp, -0.5, 0, im);
	return im;
}




static void dbread(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ','){
	std::ifstream file(filename.c_str(), ifstream::in);

	if (!file){
		string error = "No valid input file passed to dbread! ";
		CV_Error(CV_StsBadArg, error);
	}

	string line, path, label;
	while (getline(file, line))
	{
		stringstream liness(line);
		getline(liness, path, separator);
		getline(liness, label);
		if (!path.empty() && !label.empty()){
			images.push_back(unsharpMask(imread(path, 0)));
			labels.push_back(atoi(label.c_str()));
		}
	}
}

void eigenFaceTrainer(){
	vector<Mat> images;
	vector<int> labels;

	try{
		string filename = "C:/database/at.csv";
		dbread(filename, images, labels);

		cout << "size of the images is " << images.size() << endl;
		cout << "size of the labes is " << labels.size() << endl;
		cout << "Training begins...." << endl;
	}
	catch (cv::Exception& e){
		cerr << " Error opening the file " << e.msg << endl;
		exit(1);
	}

	
	Ptr<FaceRecognizer>  model = createEigenFaceRecognizer();
	
	model->train(images, labels);
	model->save("C:/database/eigenface.yml");
    cout << "Training finished...." << endl;
	
	waitKey(10000);
}

void fisherFaceTrainer()
{
	
	vector<Mat> images;
	vector<int> labels;

	try
	{		

		string filename = "C:/database/at.csv";
		dbread(filename, images, labels);

		cout << "size of the images is " << images.size() << endl;
		cout << "size of the labes is " << labels.size() << endl;
		cout << "Training begins...." << endl;
	}
	catch (cv::Exception& e)
	{
		cerr << " Error opening the file " << e.msg << endl;
		cin.get();
		exit(1);
	}


	Ptr<FaceRecognizer> model = createFisherFaceRecognizer();

	model->train(images, labels);
	int height = images[0].rows;
	model->save("C:/database/fisherface.yml");
	cout << "Training finished...." << endl;

	waitKey(10000);
}

void LBPHFaceTrainer(){

	vector<Mat> images;
	vector<int> labels;

	try{
		string filename = "C:/database/at.csv";
		dbread(filename, images, labels);

		cout << "size of the images is " << images.size() << endl;
		cout << "size of the labes is " << labels.size() << endl;
		cout << "Training begins...." << endl;
	}
	catch (cv::Exception& e){
		cerr << " Error opening the file " << e.msg << endl;
		exit(1);
	}

	Ptr<FaceRecognizer> model = createLBPHFaceRecognizer();

	model->train(images, labels);
	model->save("LBPHface.yml");
	cout << "training finished...." << endl;

	waitKey(10000);
}

int  video_FaceRecognition()
{
	
	cout << "Starting recognition...." << endl;

	Ptr<FaceRecognizer>  model = createFisherFaceRecognizer();
	model->load("C:/database/fisherface.yml");

	Mat testSample = imread("C:/database/Shahzeb/00.png", 0);
	int img_width = testSample.cols;
	int img_height = testSample.rows;

	

	string classifier = "haarcascade_frontalface_alt.xml";
	CascadeClassifier face_cascade;
	string window = "Capture - face detection";

	

	if (!face_cascade.load(classifier))
	{
		cout << " Error loading file" << endl;
		return -1;
	}



	VideoCapture cap(0);
	

	if (!cap.isOpened())
	{
		cout << "exit" << endl;
		return -1;
	}

	
	long counter = 0;

	while (true)
	{
		vector<Rect> faces;
		Mat frame;
		Mat graySacleFrame;
		Mat original;

		cap >> frame;
		counter++;

		if (!frame.empty()){

			
			original = frame.clone();
			cvtColor(original, graySacleFrame, CV_BGR2GRAY);
			face_cascade.detectMultiScale(graySacleFrame, faces, 1.1, 3, 0, cv::Size(90, 90));

			
			cout << faces.size() << " faces detected." << endl;
			std::string frameset = std::to_string(counter);
			std::string faceset = std::to_string(faces.size());

			int width = 0, height = 0;

			int label = -1;
			string person = "";

			for (int i = 0; i < faces.size(); i++)
			{
				
				Rect face_i = faces[i];
				Mat face = graySacleFrame(face_i);	
				Mat face_resized;
				cv::resize(face, face_resized, Size(img_width, img_height), 1.0, 1.0, INTER_CUBIC);

			
				label = -1; double confidence = 0;
				model->predict(face_resized, label, confidence);
				cout << " confidencde " << confidence << endl;
				string surity = to_string(confidence);
				
				rectangle(original, face_i, CV_RGB(200, 0, 10), 1);
				string text = "Detected";
				
				
				if (label == 0 || label == 1 || label == 2 || label == 3 || label == 4 || label == 5 || label == 6 || label == 7 || label == 8 || label == 9)
				{
					if (label == 0)
						person = "Abdullah Afzal";
					if (label == 1)
						person = "Unknown 1";
					if (label == 2)
						person = "Ahmad Mustafa";
					if (label == 3)
						person = "Sohaib Khalid";
					if (label == 4)
						person = "Nouman Sial";
					if (label == 5)
						person = "Obaid Satti";
					if (label == 6)
						person = "Unknown 2";
					if (label == 7)
						person = "Syed Saim";
					if (label == 8)
						person = "Nouraiz Muzaffar";
					if (label == 9)
						person = "Unknown 3";
				}
				else
				{
					person = "unknown";
				}

				int pos_x = std::max(face_i.tl().x - 10, 0);
				int pos_y = std::max(face_i.tl().y - 10, 0);

			std::string label_string = std::to_string(label);
		
			putText(original, "Person: " + person, Point(i-i, i*50+25), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(200, 0, 10), 1.0);
			putText(original, "Surity: " + surity, Point(i-i, i*50+50), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(200, 0, 10), 1.0);
		
			}

			cv::imshow("window", original);

		}

		if (waitKey(30) >= 0)
		{
		cvDestroyWindow("window");
		break;
		}
	}
}

int  photo_FaceRecognition()
{

	cout << "Starting recognition...." << endl;

	Ptr<FaceRecognizer>  model = createFisherFaceRecognizer();
	model->load("C:/database/fisherface.yml");

	Mat testSample = imread("C:/database/Shahzeb/00.png", 0);
	int img_width = testSample.cols;
	int img_height = testSample.rows;


	string classifier = "haarcascade_frontalface_alt.xml";
	CascadeClassifier face_cascade;
	string window = "Capture - face detection";



	if (!face_cascade.load(classifier))
	{
		cout << " Error loading file" << endl;
		return -1;
	}


	long counter = 0;

	
		vector<Rect> faces;
		Mat frame;
		Mat graySacleFrame;
		Mat original;
		string path="C:/shah.png";

		frame = imread(path);
		counter++;

		if (frame.empty())
		{ 
		
			cout << "Invalid Path.\n\n\n";
			cin.get();
			exit(0);
		
		}

		if (!frame.empty())
		{

				

			namedWindow(window, 1);
			original = frame.clone();
			cvtColor(original, graySacleFrame, CV_BGR2GRAY);
			face_cascade.detectMultiScale(graySacleFrame, faces, 1.1, 3, 0, cv::Size(90, 90));


			cout << faces.size() << " faces detected." << endl;
			std::string frameset = std::to_string(counter);
			std::string faceset = std::to_string(faces.size());

			int width = 0, height = 0;

			int label = -1;
			string person = "";

			for (int i = 0; i < faces.size(); i++)
			{

				Rect face_i = faces[i];
				Mat face = graySacleFrame(face_i);
				Mat face_resized;
				cv::resize(face, face_resized, Size(img_width, img_height), 1.0, 1.0, INTER_CUBIC);

				label = -1; double confidence = 0;
				model->predict(face_resized, label, confidence);
				cout << " confidencde " << confidence << endl;
				string surity = to_string(confidence);
				
				

				if (i==0){
				rectangle(original, face_i,CV_RGB(239, 255, 0) , 1);
				string text = "Detected";
				}
				else if (i==1){
				rectangle(original, face_i, CV_RGB(77, 15, 257), 1);
				string text = "Detected";
				}
				else if (i==2){
				rectangle(original, face_i, CV_RGB(200, 0, 10), 1);
				string text = "Detected";
				}
				else {
					rectangle(original, face_i, CV_RGB(1500, 159, 10), 1);
				string text = "Detected";
				}
				if (label == 0 || label == 1 || label == 2 || label == 3 || label == 4 || label == 5 || label == 6 || label == 7 || label == 8 || label == 9)
				{
					if (label == 0)
						person = "Abdullah Afzal";
					if (label == 1)
						person = "Talha Asif";
					if (label == 2)
						person = "Ahmad Mustafa";
					if (label == 3)
						person = "Sohaib Khalid";
					if (label == 4)
						person = "Nouman Sial";
					if (label == 5)
						person = "Obaid Satti";
					if (label == 6)
						person = "Another guy";
					if (label == 7)
						person = "Syed Saim";
					if (label == 8)
						person = "Nouraiz Muzaffar";
					if (label == 9)
						person = "Some guy";
				}
				else
				{
					person = "Unknown";
				}

				ofstream outfile ("C:/database/PathAndName.csv",std::ios_base::app);
				outfile <<path<<","<<person<<endl;



				int pos_x = std::max(face_i.tl().x - 10, 0);
				int pos_y = std::max(face_i.tl().y - 10, 0);

				std::string label_string = std::to_string(label);
		
				switch (i)
		{
			case 0:
			putText(original, "Person: " + person, Point(i-i, i*50+25), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(239, 255, 0), 1.0);
			putText(original, "Surity: " + surity, Point(i-i, i*50+50), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(239, 255, 0), 1.0);break;
			
		
			case 1:
		
			putText(original, "Person: " + person, Point(i-i, i*50+25), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(77, 15, 257), 1.0);
			putText(original, "Surity: " + surity, Point(i-i, i*50+50), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(77, 15, 257), 1.0);
			;break;
			case 2:
		
			putText(original, "Person: " + person, Point(i-i, i*50+25), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(200, 0, 10), 1.0);
			putText(original, "Surity: " + surity, Point(i-i, i*50+50), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(200, 0, 10), 1.0);
			;break;
			default:
				putText(original, "Person: " + person, Point(i-i, i*50+25), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(1500, 159, 10), 1.0);
			putText(original, "Surity: " + surity, Point(i-i, i*50+50), CV_FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(1500, 159, 10), 1.0);



		}

			}


			cv::imshow(window, original);

			

		}

		if (waitKey(30) >= 0) exit(0);

}

