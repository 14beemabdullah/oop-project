#include <vector>
#include <string>
#include <sstream>


class Cordinates
{

	//Private Variables 

	float x1;
	float x2;
	float y1;
	float y2;

	//Setters

public:
	
	
	void setx1(float x);
	void setx2(float x);
	
	void sety1(float x);
	void sety2(float x);
	
	//Getters
	
	float getx1();
	float getx2();
	
	float gety1();
	float gety2();

	

};


class Eye
{
public:

std:: vector <Cordinates> vectorCordinates;


};



class CSVRow
{

std::vector<std::string> m_data;
public:                                     //FUNCTIONS DEFINED IN THE CPP FILE

std::size_t size() const;
void readNextRow(std::istream& str);
std::string const& operator[](std::size_t index) const;


};

std::istream& operator>>(std::istream& str, CSVRow& data);  //DEFINED IN THE CPP FILE